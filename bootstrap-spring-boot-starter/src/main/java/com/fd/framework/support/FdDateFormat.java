package com.fd.framework.support;

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间转化类
 * -提供将多种格式的日期字符串转化为Date类型
 * -提供将Date类型转化为对外标准的日期格式,如yyyy-MM-dd HH:mm:ss
 *
 * @author XiaoLong
 * @since 1.0.0
 */
public class FdDateFormat extends SimpleDateFormat {

    private static final String BACK_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final String[] DATE_FORMATS = {
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm",
            "yyyy-MM-dd",
            "yyyy/MM/dd HH:mm:ss",
            "yyyy/MM/dd HH:mm",
            "yyyy/MM/dd"
    };

    @Override
    public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
        SimpleDateFormat sdf = new SimpleDateFormat(BACK_DATE_FORMAT);
        return new StringBuffer(sdf.format(date));
    }


    @Override
    public Date parse(String source) {
        return convertStringToDate(source);
    }

    /**
     * 将各种时间字符串转化为标准的Date类型
     *
     * @param source 目标时间字符串
     * @return {@link Date}
     * @author xiaoLong
     * @since 1.0.0
     */
    private Date convertStringToDate(String source) {
        for (String dateFormat : DATE_FORMATS) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                // 设置Lenient为false，确保严格匹配格式
                sdf.setLenient(false);
                return sdf.parse(source);
            } catch (ParseException e) {
                // 如果解析失败，尝试下一个格式
            }
        }
        throw new RuntimeException("时间格式不正确,无法解析");
    }
}
