package com.fd.framework.core;

import com.fd.framework.support.FdDateFormat;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * Fd 数据转化 - 时间转化
 * org.springframework.beans.TypeConverterDelegate#convertIfNecessary
 *
 * @author XiaoLong
 * @since 1.0.0
 */
public class FdDateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        //将多种日期格式转化为Date
        return new FdDateFormat().parse(source.trim());
    }

}
