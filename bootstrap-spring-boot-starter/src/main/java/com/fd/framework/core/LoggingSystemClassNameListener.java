package com.fd.framework.core;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;

/**
 * 日志系统类名监听器
 *
 * @author XiaoLong
 * @since 1.0.0
 */
public class LoggingSystemClassNameListener implements ApplicationListener<ApplicationStartingEvent>, Ordered {

    private static final String LOGGING_SYSTEM_NAME = "org.springframework.boot.logging.LoggingSystem";
    private static final String LOG4J2_SYSTEM_NAME = "org.springframework.boot.logging.log4j2.Log4J2LoggingSystem";

    @Override
    public void onApplicationEvent(ApplicationStartingEvent event) {
        System.setProperty(LOGGING_SYSTEM_NAME, LOG4J2_SYSTEM_NAME);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
