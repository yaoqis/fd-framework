package com.fd.framework.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 日志配置参数
 *
 * @author XiaoLong
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = "fd.log")
public class FdLogProperties {
    /**
     * 全局日志级别
     */
    private String level;
}
