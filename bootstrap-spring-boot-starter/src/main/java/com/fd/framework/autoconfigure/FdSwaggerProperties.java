package com.fd.framework.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Swagger配置
 *
 * @author XiaoLong
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = "fd.swagger")
public class FdSwaggerProperties {
    private String enable;
}
