package com.fd.framework.autoconfigure;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * fd日志配置自动配置
 *
 * @author XiaoLong
 * @since 1.0.0
 */
@EnableConfigurationProperties(value = FdLogProperties.class)
public class FdLogPropertiesAutoConfiguration {
}
