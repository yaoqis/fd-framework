#  fd-framework系统框架

## 项目构建

- [x] 建立父工程, 定义好基础依赖规范

## 一、项目启动引导模块(bootstrap-spring-boot-starter)

- [x] 引入springmvc
- [x] springmvc对请求时间进行统一格式处理 , Long精度丢失问题处理
- [x] 自定义日志文件以及日志级别
- [x] 接口文档swagger
- [ ] 全局异常处理

## 二、项目公共组件(common-spring-boot-starter)

- [ ] 自定义全局业务异常类(BizException)
- [ ] 统一响应体及其工具类
- [ ] 其他实用工具类

## 三、orm模块, mybatis / jpa(orm-spring-boot-starter)

- [ ] 集成flyway框架
- [ ] 集成myBatisPlus框架
- [ ] 定义数据库表固定字段并增加修改, 新增策略
- [ ] 构建手动事务管理器
- [ ] 集成分布式Id生成策略

## 四、分布式Id生成组件(uid-spring-boot-starter)

- [ ] 集成百度分布式id组件
- [ ] 定义分布式Id获取工具类

## 五、数据源组件(datasource-spring-boot-starter)

- [ ] 集成数据源
- [ ] 支持多数据源以及动态数据源

## 六、redis组件(redis-spring-boot-starter)

- [ ] 集成redis以及redisson
- [ ] redis进行自定义序列化处理
- [ ] 增强redis的key进行服务间的隔离

## 七、缓存组件(cache-spring-boot-starter)

- [ ] 二级缓存能力 (caffeine一级缓存, redis二级缓存)

## 八、rocketmq组件(rocketmq-spring-boot-starter)

- [ ] 提供Rocketmq消费端幂等能力
- [ ] 基于任意时间的rocketmq延时任务能力

## 九、延时任务组件

- [ ] 基于内存的延时组件能力
- [ ] 基于Redis的分布式延时任务能力

## 十、权限认证组件

- [ ] 多渠道用户体系

- [ ] 认证 , 授权 ,鉴权 , 权限控制能力
- [ ] 单点登录能力