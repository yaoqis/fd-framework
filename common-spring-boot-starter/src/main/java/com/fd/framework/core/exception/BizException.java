package com.fd.framework.core.exception;

import com.fd.framework.core.entity.BaseResultCode;

/**
 * 业务异常处理
 *
 * @author YangXiaoLong
 * @since 1.0.0
 */
public class BizException extends RuntimeException {

    private static final long serialVersionUID = -5204000613712030276L;
    private final BaseResultCode resultCode;

    public BizException(String errorMsg) {
        super(errorMsg);
        resultCode = BaseResultCode.FAILURE;
    }

    public BizException(String errorMsg, Throwable throwable) {
        super(errorMsg, throwable);
        resultCode = BaseResultCode.FAILURE;
    }

    public BizException(Throwable throwable) {
        super(throwable);
        resultCode = BaseResultCode.FAILURE;
    }

    public BizException(BaseResultCode resultCode, Throwable throwable) {
        super(throwable);
        this.resultCode = resultCode;
    }

    public BaseResultCode getResultCode() {
        return resultCode;
    }

}
