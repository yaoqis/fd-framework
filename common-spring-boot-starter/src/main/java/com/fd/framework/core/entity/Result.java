package com.fd.framework.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 统一响应结果
 *
 * @author XiaoLong
 * @since 1.0.0
 */
@Data
public class Result<T> implements Serializable {

    /**
     * 状态码
     */
    private int code;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 需要返回的数据对象
     */
    private T data;


    public Result(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public Result(T data) {
        this.data = data;
    }


    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(ResultCode resultCode, String message) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.message = message;
    }

    public Result(ResultCode resultCode, T data) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.data = data;
    }

    public static Result<Void> success() {
        return new Result<>(BaseResultCode.SUCCESS);
    }

    public static Result<Void> success(String message) {
        return new Result<>(BaseResultCode.SUCCESS, message);
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(BaseResultCode.SUCCESS, data);
    }

    public static <T> Result<T> success(ResultCode resultCode, T data) {
        return new Result<>(resultCode, data);
    }

    public static Result<Void> fail() {
        return new Result<>(BaseResultCode.FAILURE);
    }

    public static Result<Void> fail(String message) {
        return new Result<>(BaseResultCode.FAILURE, message);
    }

    public static <T> Result<T> fail(T data) {
        return new Result<>(BaseResultCode.FAILURE, data);
    }

    public static <T> Result<T> fail(ResultCode resultCode, T data) {
        return new Result<>(resultCode, data);
    }

    /**
     * 响应结果是否成功
     *
     * @return {@link Boolean} true:成功, false:失败
     * @author XiaoLong
     * @since 1.0.0
     */
    public Boolean isSuccess() {
        return this.code == BaseResultCode.SUCCESS.getCode();
    }

    /**
     * 响应结果是否失败
     *
     * @return {@link Boolean} true:失败 , false: 成功
     * @author XiaoLong
     * @since 1.0.0
     */
    public Boolean isFail() {
        return this.code == BaseResultCode.FAILURE.getCode();
    }
}
