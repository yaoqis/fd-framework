package com.fd.framework.core.entity;

/**
 * @author XiaoLong
 * @since 1.0.0
 */
public interface ResultCode {

    /**
     * 获取code
     *
     * @return {@link Integer}
     * @author XiaoLong
     * @since 1.0.0
     */
    Integer getCode();

    /**
     * 获取消息
     * @return {@link String}
     * @author XiaoLong
     * @since 1.0.0
     */
    String getMessage();
}
