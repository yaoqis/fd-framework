package com.fd.example.bootstrap;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author XiaoLong
 * @since 1.0.0
 */
@Data
@Builder
public class UserRes implements Serializable {

    private static final long serialVersionUID = 6249299783889708859L;

    private Long id;

    private String name;

    private Date birth;
}
