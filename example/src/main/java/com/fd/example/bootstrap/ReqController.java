package com.fd.example.bootstrap;

import com.fd.framework.core.entity.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 请求
 *
 * @author XiaoLong
 * @since 1.0.0
 */
@RestController
@RequestMapping("example")
public class ReqController {

    @GetMapping("user")
    public Result<UserRes> getUser() {
        UserRes res = UserRes
                .builder()
                .id(112233L)
                .name("杨晓龙")
                .birth(new Date())
                .build();
        return Result.fail(res);
    }

    @GetMapping("exception")
    public Result<UserRes> exception() {
        throw new RuntimeException("运行时异常");
    }

}
